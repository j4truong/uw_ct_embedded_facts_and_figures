<?php
/**
 * @file
 * uw_ct_embedded_facts_and_figures.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_embedded_facts_and_figures_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_embedded_facts_and_figures_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_embedded_facts_and_figures_node_info() {
  $items = array(
    'uw_embedded_facts_and_figures' => array(
      'name' => t('Embedded Facts and Figures'),
      'base' => 'node_content',
      'description' => t('Provides a visualization of facts and figures.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_embedded_facts_and_figures_paragraphs_info() {
  $items = array(
    'eff_highlighted_fact' => array(
      'name' => 'Highlighted Fact',
      'bundle' => 'eff_highlighted_fact',
      'locked' => '1',
    ),
  );
  return $items;
}
