(function() {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var factsfiguresDialog = function(editor) {
    return {
      title : 'Facts and Figures Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'factsfigures',
        label: 'factsfigures',
        elements:[{
          type: 'text',
          id: 'factsfigures-nid',
          label: 'Please enter embedded fact and figure node ID:',
          required: true,
          setup: function(element) {
            this.setValue(element.getAttribute('data-factsfigures-nid'));
          }
        },{
          type: 'select',
          id: 'usecarousel',
          label: 'Do you wish to use a carousel:',
          items: [ [ 'Yes' ], [ 'No' ]], 'default': 'Yes',
          required: true,
          setup: function(element) {
            this.setValue(element.getAttribute('data-usecarousel'));
          }
        },{
          type: 'select',
          id: 'numberpercarousel',
          label: 'If you use a carousel, how many facts/figures should be displayed at once:',
          items: [ [ '1' ], [ '2' ], [ '3' ], [ '4' ]], 'default': '3',
          required: true,
          setup: function(element) {
            this.setValue(element.getAttribute('data-numberpercarousel'));
          }
        }]
      }],
      onOk: function() {
        // Get form information.
        factsfigures_nid = this.getValueOf('factsfigures','factsfigures-nid');
        use_carousel = this.getValueOf('factsfigures','usecarousel');
        number_per_carousel = this.getValueOf('factsfigures','numberpercarousel');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (!CKEDITOR.embedded_factsfigures.factsfigures_regex.test(factsfigures_nid)) {
            errors += "You must enter a node ID.\r\n";
        }
        else {
          errors = '';
        }
        if (!factsfigures_nid) {
          errors = "You must enter a node ID.\r\n";
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before
        if(formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
         else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the facts/figures element.
          var ckfactsfiguresNode = new CKEDITOR.dom.element('ckfactsfigures');
          // Save contents of dialog as attributes of the element.
          ckfactsfiguresNode.setAttribute('data-factsfigures-nid', factsfigures_nid);
          ckfactsfiguresNode.setAttribute('data-usecarousel', use_carousel);
          ckfactsfiguresNode.setAttribute('data-numberpercarousel', number_per_carousel);

          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckfactsfigures = CKEDITOR.embedded_factsfigures.ckfactsfigures + ': ' + factsfigures_nid;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckfactsfiguresNode, 'ckfactsfigures', 'ckfactsfigures', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckfactsfigures');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          } else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckfactsfigures = CKEDITOR.embedded_factsfigures.ckfactsfigures;
        }
      },
      onShow: function() {
        // Set up to handle existing items.
        this.fakeImage = this.ckfactsfiguresNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckfactsfigures') {
          this.fakeImage = fakeImage;
          var ckfactsfiguresNode = editor.restoreRealElement(fakeImage);
          this.ckfactsfiguresNode = ckfactsfiguresNode;
          this.setupContent(ckfactsfiguresNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('factsfigures', function(editor) {
    return factsfiguresDialog(editor);
  });
})();
