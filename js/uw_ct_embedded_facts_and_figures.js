(function ($) {  

  Drupal.behaviors.uw_ct_embedded_facts_and_figures = {

    attach: function (context, settings) {

      // Start VERTICAL CHART
      // Check if there is a vertical chart on the page.
      if ( $('.field-name-field-eff-sc-type-of-chart').find('.field-item').text() ) {

        // Store the actual type of chart (Vertical, Pie, Donut).
        var type_of_chart = $('.field-name-field-eff-sc-type-of-chart').find('.field-item').text();

        // If the type of chart is Vertical, then setup the vertical chart.
        if (type_of_chart == "Vertical Chart") {

          // Hide the actually data from the form.
          $('.paragraphs-items-field-fact-figure').addClass('visuallyhidden');

          // Declare variables.
          var array_length;
          var bar_colour;
          var bar_padding;
          var bar_width;
          var counter = 0;
          var data_point_display;
          var data_point_text_size;
          var data_points = [];
          var data_points_sorted = [];
          var descriptions = [];
          var description_by_data_point = [];
          var heading_placement;
          var headings = [];
          var height;
          var i;
          var margin;
          var max_point;
          var scaler;
          var svg;
          var tip;
          var type_of_data;
          var width;

          margin = {top: 20, right: 20, bottom: 70, left: 40};

          // Place bar colour into varaible: bar_colour.
          $('.field-name-field-eff-vc-bar-colour').each(function() {
            bar_colour = ($(this).find('.field-item').text());
          });

          // Place the text size for the data point into varaible: data_point_text_size.
          data_point_text_size = $('.field-name-field-eff-vc-dp-text-size').find('.field-item').text();

          // Step through each of the rows and cells.  Store the data points, headings and descriptions.
          $('table > tbody > tr > td').each(function() {
            if (counter == 0) {
              data_points.push($(this).text());
            }

            if (counter == 1) {
              headings.push($(this).text());
            }

            if (counter == 2) {
              descriptions.push($(this).text());
              counter = -1;
            }
            counter++;
          });

          // Loop through all data and store the descriptions into associative
          // array: descriptions_by_data_point using the data_point of every
          // entry as the key.  This is done so that descriptions can be easily
          // and readily available by reference through the data point.
          for(i = 0; i < descriptions.length; i++) {
            description_by_data_point[ data_points[i] ] = descriptions[i];
          }

          // If there are descriptions entered then place note on page to hover
          // over bars to see the description of each bar.
          if (descriptions.length > 0) {
            $(".content_node").after("<div id='description-help-text'><p>Hover over bars to see description.</p></div>");
          }

          // Add the SVG to page
          svg = d3.select(".content_node").append("svg");

          // Declare the tip which is actually the description.
          tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function(f) {
              return description_by_data_point[f];
            });
  
          // Add the tip to the SVG
          svg.call(tip);

          // Store and get the width/height from the parent DOM.
          width = $("svg").parent().width();
          height = $("svg").parent().height();

          // Store and get the bar_width and bar_padding
          bar_width = width / headings.length;
          bar_padding = 1;

          // Add width and height to the SVG
          svg.attr("width", width);
          svg.attr("height", height + margin.top + margin.bottom);

          // Create and array that has values of the data points so that it can be sorted.
          for (i = 0; i < data_points.length; i++) {
            data_points_sorted.push(data_points[i]);
          }

          // Sort the array so that can grab max and min values.
          data_points_sorted.sort(function(a, b) {
            return a - b;
          });

          // Get the array length, maximum value (which is the last value in the sorted array)
          // and then get the scaler that will be used to scale the bars in the chart.
          array_length = data_points.length - 1;
          max_point = data_points_sorted[array_length];
          scaler = max_point / height;

          // Add each of the bars to the SVG
          svg.selectAll("rect")
              .data(data_points)
              .enter()
              .append("rect")
              .attr("fill", '#'+bar_colour)
              .attr("x", function(d, i) {
                return i * bar_width;
              })
              .attr("y", function(d) { return height - (d / scaler); })
              .attr("width", bar_width - bar_padding)
              .attr("height", function(d) { return (d / scaler); } )
              .on('mouseover', tip.show)
              .on('mouseout', tip.hide);

          // Add each of the headings to the SVG
          svg.selectAll("headings")
            .data(headings)
            .enter()
            .append("text")
            .text(function(d) {
              return d;
            })
            .attr("x", function(d, i) {
              return i * bar_width + (bar_width - bar_padding) / 2;
            })
            .attr("y", function (d) {
              return height + margin.bottom;
            })
            .attr("text-anchor", "middle");

          // Add the data points to the SVG, also accounting for the type of data.
          svg.selectAll("data_points")
            .data(data_points)
            .enter()
            .append("text")
            .text(function(d) {
              switch(type_of_data) {
                case "Currency ($)":
                  data_point_display = "$" + d;
                  break;
                case "Percentage (%)":
                  data_point_display = d + "%";
                  break;
              default:
                  data_point_display = d;
              }
              return data_point_display;
            })
            .attr("x", function(d, i) {
              return i * bar_width + (bar_width - bar_padding) / 2;
            })
            .attr("y", function (d) {
              return height + margin.top + (data_point_text_size / 2);
            })
            .attr("text-anchor", "middle")
            .attr("font-family", "sans-serif")
            .attr("font-size", data_point_text_size + "px")
            .attr("fill", "black");
  
          // Add the rsize so that the SVG is responsive.
          //d3.select(window).on('resize', resize);
        }
      }
      // End VERTICAL CHART

      // Get variable to see if a carousel is to be used.
      var data_usecarousel = $(".data-usecarousel").text();

      if (data_usecarousel == "Yes") {
        // Get the number of facts/figures for each carousel.
        var data_numberpercarousel = $(".data-numberpercarousel").text();

        // Setup carousel.
        var highlightedFact = $(".highlighted-fact-wrapper");
          highlightedFact.owlCarousel({
          items : data_numberpercarousel,
          itemsDesktop : [1024,data_numberpercarousel],
          itemsTablet : [768,2],
          itemsMobile : [480,1],
          navigation : true,
          navigationText : ["&lsaquo;&lsaquo;&nbsp;prev","next&nbsp;&rsaquo;&rsaquo;"]
        }); 
      }
   }

 };})(jQuery);
