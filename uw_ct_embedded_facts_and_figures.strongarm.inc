<?php
/**
 * @file
 * uw_ct_embedded_facts_and_figures.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_embedded_facts_and_figures_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_embedded_facts_and_figures';
  $strongarm->value = '0';
  $export['comment_anonymous_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_embedded_facts_and_figures';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_embedded_facts_and_figures';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_embedded_facts_and_figures';
  $strongarm->value = 1;
  $export['comment_form_location_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_embedded_facts_and_figures';
  $strongarm->value = '1';
  $export['comment_preview_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_embedded_facts_and_figures';
  $strongarm->value = 1;
  $export['comment_subject_field_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_embedded_facts_and_figures';
  $strongarm->value = '1';
  $export['comment_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_embedded_facts_and_figures';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '30',
        ),
        'metatags' => array(
          'weight' => '40',
        ),
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
        'redirect' => array(
          'weight' => '30',
        ),
        'xmlsitemap' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_embedded_facts_and_figures';
  $strongarm->value = '0';
  $export['language_content_type_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_embedded_facts_and_figures';
  $strongarm->value = array();
  $export['menu_options_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_embedded_facts_and_figures';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_embedded_facts_and_figures';
  $strongarm->value = array(
    0 => 'status',
    1 => 'revision',
  );
  $export['node_options_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_embedded_facts_and_figures';
  $strongarm->value = '1';
  $export['node_preview_uw_embedded_facts_and_figures'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_embedded_facts_and_figures';
  $strongarm->value = 0;
  $export['node_submitted_uw_embedded_facts_and_figures'] = $strongarm;

  return $export;
}
