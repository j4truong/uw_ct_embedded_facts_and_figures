<?php
/**
 * @file
 * Code for the Embedded Facts and Figures feature.
 */

include_once 'uw_ct_embedded_facts_and_figures.features.inc';

function uw_ct_embedded_facts_and_figures_preprocess_html(&$variables) {
  // Add css for facts and figures
  $module_path = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures');

  drupal_add_css($module_path . '/css/uw_ct_embedded_facts_and_figures.css');
  drupal_add_css($module_path . '/css/highlighted_fact.css');
}

function uw_ct_embedded_facts_and_figures_wysiwyg_plugin($editor, $version) {
  if ($editor === 'ckeditor') {
    return array('embedded_factsfigures' => array(
      'path' => drupal_get_path('module', 'uw_ct_embedded_facts_and_figures') . '/plugins/embedded_facts_and_figures',
      'filename' => 'plugin.js',
      'load' => TRUE,
      'buttons' => array(
        'Facts and Figures' => t('Facts and Figures'),
      ),
    ));
  }
}

/**
 * Implements hook_filter_info().
 * Provide filter to convert custom cktwitter/ckfacebook elements to embed object.
 */
function uw_ct_embedded_facts_and_figures_filter_info() {  
  $filters['ckeditor_embedded_ff'] = array(
    'title' => t('CKEditor Embedded Facts and Figures'),
    'description' => t('Converts embedded elements into widgets.'),
    'process callback' => 'cke_ff_process',
  );
  return $filters;
}


function cke_ff_process($text, $filter, $format, $langcode, $cache, $cache_id) {
  // Add the javascript to facts and figures
  $module_path = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures');
  $library_path = libraries_get_path('d3');
  
  drupal_add_js($library_path .'/d3.min.js', 'file');

  drupal_add_js($module_path .'/js/owl.carousel.js', 'file');
  drupal_add_js($module_path .'/js/uw_ct_embedded_facts_and_figures.js', 'file');

  drupal_add_js($module_path .'/js/d3_tip.js', 'file');

  // Skip filter process on non node edit pages.
  if (arg(0) == 'node' && arg(2) == 'edit') {
    return;
  }
  $path = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures');

  // Get cktimeline tags.
  unset($matches);
  if (!preg_match_all('`</?ck(?:factsfigures)\b.*?>`is', $text, $matches, PREG_OFFSET_CAPTURE)) {
    //if there aren't any, don't process any further
    return $text;
  }

  // Get target elements, ignoring nested ones.
  $elements = array();
  $i = -1;
  $depth = 0;
  foreach ($matches[0] as $match) {
    // Empty tag
    if (drupal_substr($match[0], -2, 2) === '/>') {
      if ($depth === 0) {
        $elements[++$i]['start'] = $match;
        $elements[$i]['end'] = $match;
      }
    }
    // Open tag
    elseif (drupal_substr($match[0], 0, 2) !== '</') {
      if ($depth === 0) {
        $elements[++$i]['start'] = $match;
      }
      $depth++;
    }
    // Close tag
    elseif ($depth > 0) {
      $depth--;
      if ($depth === 0) {
        $elements[$i]['end'] = $match;
      }
    }
  }

  // Parse cktwitter/ckfacebook/etc. elements.
  foreach (array_keys($elements) as $key) {
    //determine what type of tag this is
    $mediatype = substr($elements[$key]['start'][0],1,strpos($elements[$key]['start'][0],' ')-1);

    //get the attributes on the tag (note that if the attribute contains non a-z characters, only the final a-z characters will be returned);
    $elements[$key]['attributes'] = ckeditor_swf_parse_attributes($elements[$key]['start'][0]);

    //set up the replacement elements
    $attributes_div = array();
    $attributes_div['class'] = $mediatype;
    foreach($elements[$key]['attributes'] as $attr => $attrvalue) {
      if ($attr == 'nid') {
        $attr = 'widget-id';
      }
      $attributes_div['data-'.$attr] = $attrvalue;
    }

    if ($mediatype == 'ckfactsfigures' && !empty($elements[$key]['attributes']['nid'])) {
      if (module_exists('uw_ct_embedded_facts_and_figures') && (uw_ct_embedded_facts_and_figures_get_node_count('uw_embedded_facts_and_figures') > 0)) {
        // Get nid from the input
        $nid = uw_ct_embedded_facts_and_figures_sourcecode_get_nid($elements[$key]['attributes']['nid']);

        // If $nid is exist, embeded the embedded_facts_and_figures view into this node with this nid to display, 
        // otherwise display wrong message.
        // A view was created and needed here, because grabbing the node and either using the node or using
        // drupal_render to display the node_view would cause PHP memory limit errors on edits and drafts.
        if ($nid) {
          $fact_point_present = true;
          $view_result = views_get_view_result('embedded_facts_and_figures', 'facts_and_figures_page', $nid);
          $counter = 0;

          foreach($view_result[0]->field_field_fact_figure[0]['rendered']['entity']['paragraphs_item'] as $vr) {
            $background_colour = $vr['field_eff_hf_background_colour'][0]['#markup'];

            $default_colour = $vr['field_eff_hf_default_colour'][0]['#markup'];
            $start  = strpos($default_colour, '(');
            $end    = strpos($default_colour, ')', $start + 1);
            $length = $end - $start;
            $default_colour_class = strtolower(substr($default_colour, $start + 1, $length - 1));

            $text_align = $vr['field_eff_hf_text_align'][0]['#markup'];

            while($fact_point_present) {
              $fact_point = $vr['field_eff_hf_fact_points'][$counter];
              
              foreach($fact_point['entity']['field_collection_item'] as $fp) {
                if(isset($fp['field_eff_hf_use_icon'][0]['#markup'])){
                  if($fp['field_eff_hf_use_icon'][0]['#markup'] == "Yes") {
                    $data[$counter]['icon'] = file_create_url($fp['field_eff_hf_icon']['#items'][0]['uri']);
                  }
                }
                else {
                  $data[$counter]['icon'] = NULL;
                }

                if(isset($fp['field_eff_hf_caption'][0]['#markup'])) {
                  $data[$counter]['caption'] = $fp['field_eff_hf_caption'][0]['#markup'];
                }
                else {
                  $data[$counter]['caption'] = '';
                }

                $text_counter = 0;
              if(array_key_exists($text_counter, $fp['field_eff_hf_text'])) {
                $text_present_flag = true;
              }
              else {
                $text_present_flag = false;
              }

                while($text_present_flag) {
                  foreach($fp['field_eff_hf_text'][$text_counter]['entity']['field_collection_item'] as $text_items) {
                    $data[$counter]['text'][$text_counter]['text_text'] = $text_items['field_eff_hf_text_text'][0]['#markup'];
                    $data[$counter]['text'][$text_counter]['text_type'] = $text_items['field_eff_hf_text_type_of_text'][0]['#markup'];
                  }
                  $text_counter++;
                  if (array_key_exists($text_counter, $fp['field_eff_hf_text'])) {
                    $text_present_flag = true;
                  }
                  else {
                    $text_present_flag = false;
                  }
                }
              }
              $counter++;
              if (array_key_exists($counter, $vr['field_eff_hf_fact_points'])) {
                $fact_point_present = true;
              }
              else {
                $fact_point_present = false;
              }
            }
          }

          preg_match_all('`data-usecarousel=\"(.*)\"`', $text, $matches, PREG_OFFSET_CAPTURE);
          $data_usecarousel = '';
          if(is_array($matches) && isset($matches[0][0][0])) {
            $data_usecarousel_delete = substr($matches[0][0][0], 0, (stripos($matches[0][0][0], '"')+1));
            $data_usecarousel = str_replace($data_usecarousel_delete, '', $matches[0][0][0]);
            $data_usecarousel = str_replace('"', '', $data_usecarousel);
          }

          preg_match_all('`data-numberpercarousel=\"(.*)\"`', $text, $matches, PREG_OFFSET_CAPTURE);
          $data_numberpercarousel_delete = substr($matches[0][0][0], 0, (stripos($matches[0][0][0], '"')+1));
          $data_numberpercarousel = str_replace($data_numberpercarousel_delete, '', $matches[0][0][0]);
          $data_numberpercarousel_delete = substr($data_numberpercarousel, stripos($data_numberpercarousel, '"'), strlen($data_numberpercarousel));
          $data_numberpercarousel = str_replace($data_numberpercarousel_delete, '', $data_numberpercarousel);

          $html = '<p hidden class="data-usecarousel">'.$data_usecarousel.'</p>';
          $html .= '<p hidden class="data-numberpercarousel">'.$data_numberpercarousel.'</p>';
          if($data_usecarousel == "Yes")
            $html .= '<div class="highlighted-fact-wrapper owl-carousel owl-theme" data-highlighted-carousel>';
          else
            $html .= '<div class="highlighted-fact-wrapper">';

          foreach($data as $data_item) {
            if($data_item['icon'] !== NULL) {
              $html .= '<div class="highlighted-fact text-' . strtolower($text_align) . ' ' . $default_colour_class . '">';
              $html .= '<img src="' . $data_item['icon'] . '">';
            }
            else {
               $html .= '<div class="highlighted-fact text-' . strtolower($text_align) . ' ' . $default_colour_class . ' no-icon">';
            }
            foreach($data_item['text'] as $di) {              
              $html .= '<span class="highlighted-fact-text-' . strtolower($di['text_type']) . '">' . $di['text_text'] . "</span>";
            }
            if($data_item['caption'] !== '') {
              $html .= '<span class="highlighted-fact-caption">' . $data_item['caption'] . '</span>';
            }
            $html .= '</div>';
          }

          $html .= '</div>';

          $elements[$key]['output'] = $html;
        } else {
          $elements[$key]['output'] = 'An incorrect node ID was entered in the Facts and Figures properties.';
        }
      } else {
        $elements[$key]['output'] = 'The Embedded Facts and Figures must first be enabled before a new timeline can be created.';
      }
    }
  }

  $start = 0;
  $output = '';
  foreach ($elements as $element) {
    // Must use substr() not mb_substr() or drupal_substr() because preg_match_all with PREG_OFFSET_CAPTURE returns byte offsets, not character offsets.
    $output .= substr($text, $start, $element['start'][1] - $start);
    $output .= $element['output'];
    $start = $element['end'][1] + drupal_strlen($element['end'][0]);
  }
  $output .= substr($text, $start);

  return $output;

}

/**
 * Get nid from the given restful URL, and validate 
 * the content type of this node is embedded timeline.
 **/
function uw_ct_embedded_facts_and_figures_sourcecode_get_nid($nid) {
  $uw_embedded_facts_and_figures_nid = NULL;
  preg_match('/[1-9]+[0-9]*$/', $nid, $matches);
  if (isset($matches[0])) {
    $node = node_load($nid);
    if (isset($node) && is_object($node) && $node->type == 'uw_embedded_facts_and_figures') {
      $uw_embedded_facts_and_figures_nid = $nid;
    }
  }
  return $uw_embedded_facts_and_figures_nid;
}

/**
 * Check if there are uw_embedded_timeline contents
 */
function uw_ct_embedded_facts_and_figures_get_node_count($content_type) {
  $query = "SELECT COUNT(*) amount FROM {node} n ".
      "WHERE n.type = :type";
  $result = db_query($query, array(':type' => $content_type))->fetch();
  return $result->amount;
}
