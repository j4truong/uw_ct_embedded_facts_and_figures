<?php
/**
 * @file
 * uw_ct_embedded_facts_and_figures.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_embedded_facts_and_figures_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'embedded_facts_and_figures';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Embedded Facts and Figures';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Facts and Figures';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Fact/Figure */
  $handler->display->display_options['fields']['field_fact_figure']['id'] = 'field_fact_figure';
  $handler->display->display_options['fields']['field_fact_figure']['table'] = 'field_data_field_fact_figure';
  $handler->display->display_options['fields']['field_fact_figure']['field'] = 'field_fact_figure';
  $handler->display->display_options['fields']['field_fact_figure']['settings'] = array(
    'view_mode' => 'full',
  );
  $handler->display->display_options['fields']['field_fact_figure']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_embedded_facts_and_figures' => 'uw_embedded_facts_and_figures',
  );

  /* Display: Facts and Figures */
  $handler = $view->new_display('page', 'Facts and Figures', 'facts_and_figures_page');
  $handler->display->display_options['path'] = 'embedded-facts-and-figures';
  $translatables['embedded_facts_and_figures'] = array(
    t('Master'),
    t('Facts and Figures'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Fact/Figure'),
    t('All'),
  );
  $export['embedded_facts_and_figures'] = $view;

  return $export;
}
