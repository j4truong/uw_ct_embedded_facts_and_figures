<?php
/**
 * @file
 * uw_ct_embedded_facts_and_figures.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_embedded_facts_and_figures_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eff_hf_fact_point|field_collection_item|field_eff_hf_fact_points|form';
  $field_group->group_name = 'group_eff_hf_fact_point';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_eff_hf_fact_points';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fact point',
    'weight' => '33',
    'children' => array(
      0 => 'field_eff_hf_use_icon',
      1 => 'field_eff_hf_icon',
      2 => 'field_eff_hf_use_caption',
      3 => 'field_eff_hf_caption',
      4 => 'group_eff_hf_fact_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-eff-hf-fact-point field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_eff_hf_fact_point|field_collection_item|field_eff_hf_fact_points|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eff_hf_fact_text|field_collection_item|field_eff_hf_fact_points|form';
  $field_group->group_name = 'group_eff_hf_fact_text';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_eff_hf_fact_points';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_eff_hf_fact_point';
  $field_group->data = array(
    'label' => 'Fact Text',
    'weight' => '33',
    'children' => array(
      0 => 'field_eff_hf_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-eff-hf-fact-text field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_eff_hf_fact_text|field_collection_item|field_eff_hf_fact_points|form'] = $field_group;

  return $export;
}
